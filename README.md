# Animated Header Demo

A collapsible header demostrating the use of the 'Animated' library for React Native.

Author: J. Araujo - 2019 

![screenshot](https://bitbucket.org/jota_araujo/react-native-animated-header/raw/4ac41d4c7ae2df30901d31423e73abd2e314d2b0/screenshots/animated2.gif)