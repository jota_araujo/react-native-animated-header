import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  Animated,
  FlatList,
  Text,
  Platform,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import { Constants, LinearGradient, ScreenOrientation } from 'expo';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import {
  material,
  systemWeights,
  materialColors,
} from 'react-native-typography';

import tracks from './tracks';
import TrackList from './components/TrackList';
import SmallHeader from './components/SmallHeader';
import getInterpolations from './get-interpolations';

export default class App extends React.Component {
  state = {
    scrollY: new Animated.Value(0),
  };
  componentWillMount() {
    ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
  }
  render() {
    let {
      translateY,
      translateY2,
      scale,
      opacity,
      circleOpacity,
    } = getInterpolations(this.state.scrollY);

    //this.state.scrollY.addListener(console.log);

    return (
      <View style={styles.root}>
        <Animated.ScrollView
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            { useNativeDriver: true }
          )}
          contentContainerStyle={{  paddingTop: 380 }}
          style={{ flex: 1 }}>
          <TrackList tracks={tracks} />
        </Animated.ScrollView>
        <Animated.View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            height: 380,
            opacity: opacity,
            transform: [{ translateY }],
          }}>
          <Image
            source={require('./assets/dualipa_profile.jpg')}
            style={{ width: null, height: 380, left: 0, right: 0 }}
          />
          <LinearGradient
            colors={['#00000000', '#000000']}
            style={{
              position: 'absolute',
              top: 310,
              left: 0,
              right: 0,
              height: 70,
            }}
          />
        </Animated.View>
        <SmallHeader opacity={circleOpacity} />
        <Animated.View
          style={{
            backgroundColor: 'transparent',
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            height: 380,
            alignItems: 'center',
            justifyContent: 'flex-end',
            transform: [{ translateY: translateY2 }],
          }}>
          <Text
            style={{
              backgroundColor: 'transparent',
              fontSize: 19,
              paddingBottom: 8,
              color: '#fff',
            }}>
            Dua Lipa
          </Text>
        </Animated.View>
        <StatusBar barStyle="light-content" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'black',
  },
});
