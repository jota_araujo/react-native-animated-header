export default scrollY => {
  return {
    translateY: scrollY.interpolate({
      inputRange: [0, 380],
      outputRange: [0, -380],
      extrapolate: 'clamp',
    }),

    translateY2: scrollY.interpolate({
      inputRange: [0, 230],
      outputRange: [0, -230],
      extrapolate: 'clamp',
    }),

    scale: scrollY.interpolate({
      inputRange: [0, 380],
      outputRange: [1, 0.5],
      extrapolate: 'clamp',
    }),

    opacity: scrollY.interpolate({
      inputRange: [0, 380],
      outputRange: [2, -1],
      extrapolate: 'clamp',
    }),

    circleOpacity: scrollY.interpolate({
      inputRange: [0, 380],
      outputRange: [-1, 1.5],
      extrapolate: 'clamp',
    }),
  };
};
