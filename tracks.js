export default [
  {
    id: 482990352,
    readable: true,
    title: 'One Kiss',
    title_short: 'One Kiss',
    title_version: '',
    link: 'https://www.deezer.com/track/482990352',
    duration: 214,
    rank: 992529,
    explicit_lyrics: false,
    explicit_content_lyrics: 0,
    explicit_content_cover: 0,
    preview:
      'https://cdns-preview-2.dzcdn.net/stream/c-2c60c73eea404a623174c7a694881e27-4.mp3',
    contributors: [
      {
        id: 12178,
        name: 'Calvin Harris',
        link: 'https://www.deezer.com/artist/12178',
        share:
          'https://www.deezer.com/artist/12178?utm_source=deezer&utm_content=artist-12178&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/12178/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/fa57fb13c77a51c68374f9e98686ee7d/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/fa57fb13c77a51c68374f9e98686ee7d/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/fa57fb13c77a51c68374f9e98686ee7d/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/fa57fb13c77a51c68374f9e98686ee7d/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/12178/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
    ],
    artist: {
      id: 12178,
      name: 'Calvin Harris',
      tracklist: 'https://api.deezer.com/artist/12178/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 60712222,
      title: 'One Kiss',
      cover: 'https://api.deezer.com/album/60712222/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/0397baea24f861db7ee63fb1c70391f9/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/0397baea24f861db7ee63fb1c70391f9/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/0397baea24f861db7ee63fb1c70391f9/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/0397baea24f861db7ee63fb1c70391f9/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/60712222/tracks',
      type: 'album',
    },
    type: 'track',
  },
  {
    id: 550232732,
    readable: true,
    title: 'Electricity',
    title_short: 'Electricity',
    title_version: '',
    link: 'https://www.deezer.com/track/550232732',
    duration: 238,
    rank: 993173,
    explicit_lyrics: false,
    explicit_content_lyrics: 0,
    explicit_content_cover: 0,
    preview:
      'https://cdns-preview-9.dzcdn.net/stream/c-9e628f149ed0413087cd01dcec713c93-3.mp3',
    contributors: [
      {
        id: 14890083,
        name: 'Silk City',
        link: 'https://www.deezer.com/artist/14890083',
        share:
          'https://www.deezer.com/artist/14890083?utm_source=deezer&utm_content=artist-14890083&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/14890083/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/3e8d73251751d9181049644e5543a5c7/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/3e8d73251751d9181049644e5543a5c7/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/3e8d73251751d9181049644e5543a5c7/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/3e8d73251751d9181049644e5543a5c7/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/14890083/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
      {
        id: 1846,
        name: 'Diplo',
        link: 'https://www.deezer.com/artist/1846',
        share:
          'https://www.deezer.com/artist/1846?utm_source=deezer&utm_content=artist-1846&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/1846/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9e6c448d56cbfdcae299be9a5a8769f6/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9e6c448d56cbfdcae299be9a5a8769f6/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9e6c448d56cbfdcae299be9a5a8769f6/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9e6c448d56cbfdcae299be9a5a8769f6/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/1846/top?limit=50',
        type: 'artist',
        role: 'Featured',
      },
      {
        id: 13204,
        name: 'Mark Ronson',
        link: 'https://www.deezer.com/artist/13204',
        share:
          'https://www.deezer.com/artist/13204?utm_source=deezer&utm_content=artist-13204&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/13204/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/cacdbfdf5fa8c600eadc3e1c2b6ac8c8/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/cacdbfdf5fa8c600eadc3e1c2b6ac8c8/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/cacdbfdf5fa8c600eadc3e1c2b6ac8c8/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/cacdbfdf5fa8c600eadc3e1c2b6ac8c8/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/13204/top?limit=50',
        type: 'artist',
        role: 'Featured',
      },
    ],
    artist: {
      id: 14890083,
      name: 'Silk City',
      tracklist: 'https://api.deezer.com/artist/14890083/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 72333032,
      title: 'Electricity',
      cover: 'https://api.deezer.com/album/72333032/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/f948ee6c4844c0451ad64c75e19c028f/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/f948ee6c4844c0451ad64c75e19c028f/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/f948ee6c4844c0451ad64c75e19c028f/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/f948ee6c4844c0451ad64c75e19c028f/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/72333032/tracks',
      type: 'album',
    },
    type: 'track',
  },
  {
    id: 366297871,
    readable: true,
    title: 'IDGAF',
    title_short: 'IDGAF',
    title_version: '',
    link: 'https://www.deezer.com/track/366297871',
    duration: 217,
    rank: 933884,
    explicit_lyrics: true,
    explicit_content_lyrics: 1,
    explicit_content_cover: 0,
    preview:
      'https://cdns-preview-5.dzcdn.net/stream/c-5f1ed1aaab7d4dc765781a9bf16c95ac-3.mp3',
    contributors: [
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
    ],
    artist: {
      id: 8706544,
      name: 'Dua Lipa',
      tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 42194891,
      title: 'Dua Lipa (Deluxe)',
      cover: 'https://api.deezer.com/album/42194891/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/42194891/tracks',
      type: 'album',
    },
    type: 'track',
  },
  {
    id: 620692342,
    readable: true,
    title: 'Swan Song (From the Motion Picture "Alita: Battle Angel")',
    title_short: 'Swan Song (From the Motion Picture "Alita: Battle Angel")',
    title_version: '',
    link: 'https://www.deezer.com/track/620692342',
    duration: 182,
    rank: 978249,
    explicit_lyrics: false,
    explicit_content_lyrics: 0,
    explicit_content_cover: 2,
    preview:
      'https://cdns-preview-8.dzcdn.net/stream/c-8adfa23297d0800cdc4c198b5d15766d-3.mp3',
    contributors: [
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
    ],
    artist: {
      id: 8706544,
      name: 'Dua Lipa',
      tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 84978632,
      title: 'Swan Song (From the Motion Picture "Alita: Battle Angel")',
      cover: 'https://api.deezer.com/album/84978632/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/dcec28827c029ef7e85def89683cd7ad/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/dcec28827c029ef7e85def89683cd7ad/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/dcec28827c029ef7e85def89683cd7ad/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/dcec28827c029ef7e85def89683cd7ad/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/84978632/tracks',
      type: 'album',
    },
    type: 'track',
  },
  {
    id: 366297921,
    readable: true,
    title: 'New Rules',
    title_short: 'New Rules',
    title_version: '',
    link: 'https://www.deezer.com/track/366297921',
    duration: 209,
    rank: 950118,
    explicit_lyrics: false,
    explicit_content_lyrics: 0,
    explicit_content_cover: 0,
    preview:
      'https://cdns-preview-c.dzcdn.net/stream/c-cab3cdc9503765922c3aeb05e2b0d0f6-3.mp3',
    contributors: [
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
    ],
    artist: {
      id: 8706544,
      name: 'Dua Lipa',
      tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 42194891,
      title: 'Dua Lipa (Deluxe)',
      cover: 'https://api.deezer.com/album/42194891/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/42194891/tracks',
      type: 'album',
    },
    type: 'track',
  },
  {
    id: 568846222,
    readable: true,
    title: 'Kiss and Make Up',
    title_short: 'Kiss and Make Up',
    title_version: '',
    link: 'https://www.deezer.com/track/568846222',
    duration: 190,
    rank: 939860,
    explicit_lyrics: false,
    explicit_content_lyrics: 0,
    explicit_content_cover: 0,
    preview:
      'https://cdns-preview-f.dzcdn.net/stream/c-f07928e07ad51da1d536953e8cc31161-2.mp3',
    contributors: [
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
      {
        id: 10803980,
        name: 'BLACKPINK',
        link: 'https://www.deezer.com/artist/10803980',
        share:
          'https://www.deezer.com/artist/10803980?utm_source=deezer&utm_content=artist-10803980&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/10803980/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/26576b6f04a048eb2514280c773ecdda/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/26576b6f04a048eb2514280c773ecdda/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/26576b6f04a048eb2514280c773ecdda/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/26576b6f04a048eb2514280c773ecdda/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/10803980/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
    ],
    artist: {
      id: 8706544,
      name: 'Dua Lipa',
      tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 75775892,
      title: 'Dua Lipa (Complete Edition)',
      cover: 'https://api.deezer.com/album/75775892/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/a0276b18d24c80be6968de9ef9d1a298/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/a0276b18d24c80be6968de9ef9d1a298/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/a0276b18d24c80be6968de9ef9d1a298/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/a0276b18d24c80be6968de9ef9d1a298/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/75775892/tracks',
      type: 'album',
    },
    type: 'track',
  },
  {
    id: 140856305,
    readable: true,
    title: 'Scared to Be Lonely',
    title_short: 'Scared to Be Lonely',
    title_version: '',
    link: 'https://www.deezer.com/track/140856305',
    duration: 220,
    rank: 896527,
    explicit_lyrics: false,
    explicit_content_lyrics: 0,
    explicit_content_cover: 0,
    preview:
      'https://cdns-preview-2.dzcdn.net/stream/c-2a617220ac4fec839455b061b5f2fbe1-5.mp3',
    contributors: [
      {
        id: 3968561,
        name: 'Martin Garrix',
        link: 'https://www.deezer.com/artist/3968561',
        share:
          'https://www.deezer.com/artist/3968561?utm_source=deezer&utm_content=artist-3968561&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/3968561/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/c53b684dc60abbde2a50afe9a524c863/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/c53b684dc60abbde2a50afe9a524c863/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/c53b684dc60abbde2a50afe9a524c863/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/c53b684dc60abbde2a50afe9a524c863/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/3968561/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
    ],
    artist: {
      id: 3968561,
      name: 'Martin Garrix',
      tracklist: 'https://api.deezer.com/artist/3968561/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 15186629,
      title: 'Scared to Be Lonely',
      cover: 'https://api.deezer.com/album/15186629/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/8e6e0c8973442986572a2e8a5492fdd9/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/8e6e0c8973442986572a2e8a5492fdd9/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/8e6e0c8973442986572a2e8a5492fdd9/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/8e6e0c8973442986572a2e8a5492fdd9/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/15186629/tracks',
      type: 'album',
    },
    type: 'track',
  },
  {
    id: 109843102,
    readable: true,
    title: 'Be The One',
    title_short: 'Be The One',
    title_version: '',
    link: 'https://www.deezer.com/track/109843102',
    duration: 202,
    rank: 843017,
    explicit_lyrics: false,
    explicit_content_lyrics: 0,
    explicit_content_cover: 0,
    preview:
      'https://cdns-preview-8.dzcdn.net/stream/c-86d0db135b34b697cb765b877fbb7450-5.mp3',
    contributors: [
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
    ],
    artist: {
      id: 8706544,
      name: 'Dua Lipa',
      tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 11443400,
      title: 'Be The One',
      cover: 'https://api.deezer.com/album/11443400/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/1b2c539c2f533d848d08a7896edae76f/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/1b2c539c2f533d848d08a7896edae76f/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/1b2c539c2f533d848d08a7896edae76f/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/1b2c539c2f533d848d08a7896edae76f/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/11443400/tracks',
      type: 'album',
    },
    type: 'track',
  },
  {
    id: 457299792,
    readable: true,
    title: 'High',
    title_short: 'High',
    title_version: '',
    link: 'https://www.deezer.com/track/457299792',
    duration: 196,
    rank: 687109,
    explicit_lyrics: true,
    explicit_content_lyrics: 1,
    explicit_content_cover: 1,
    preview:
      'https://cdns-preview-a.dzcdn.net/stream/c-aab8b34a6cccccac0f3b1c3f8d97a5f6-3.mp3',
    contributors: [
      {
        id: 10668247,
        name: 'Whethan',
        link: 'https://www.deezer.com/artist/10668247',
        share:
          'https://www.deezer.com/artist/10668247?utm_source=deezer&utm_content=artist-10668247&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/10668247/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/2178d5dc80a48a4d9ab403566f665e4c/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/2178d5dc80a48a4d9ab403566f665e4c/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/2178d5dc80a48a4d9ab403566f665e4c/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/2178d5dc80a48a4d9ab403566f665e4c/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/10668247/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
    ],
    artist: {
      id: 10668247,
      name: 'Whethan',
      tracklist: 'https://api.deezer.com/artist/10668247/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 56252602,
      title: 'Fifty Shades Freed (Original Motion Picture Soundtrack)',
      cover: 'https://api.deezer.com/album/56252602/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/136d036827c5ae936056d37839a80d2a/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/136d036827c5ae936056d37839a80d2a/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/136d036827c5ae936056d37839a80d2a/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/136d036827c5ae936056d37839a80d2a/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/56252602/tracks',
      type: 'album',
    },
    type: 'track',
  },
  {
    id: 549967572,
    readable: true,
    title: 'Want To',
    title_short: 'Want To',
    title_version: '',
    link: 'https://www.deezer.com/track/549967572',
    duration: 211,
    rank: 803942,
    explicit_lyrics: false,
    explicit_content_lyrics: 0,
    explicit_content_cover: 0,
    preview:
      'https://cdns-preview-e.dzcdn.net/stream/c-e55f4658ffe138166cfde0f17c7ee90f-3.mp3',
    contributors: [
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
    ],
    artist: {
      id: 8706544,
      name: 'Dua Lipa',
      tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 72288452,
      title: 'Want To',
      cover: 'https://api.deezer.com/album/72288452/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/a0276b18d24c80be6968de9ef9d1a298/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/a0276b18d24c80be6968de9ef9d1a298/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/a0276b18d24c80be6968de9ef9d1a298/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/a0276b18d24c80be6968de9ef9d1a298/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/72288452/tracks',
      type: 'album',
    },
    type: 'track',
  },
  {
    id: 366297881,
    readable: true,
    title: 'Blow Your Mind (Mwah)',
    title_short: 'Blow Your Mind (Mwah)',
    title_version: '',
    link: 'https://www.deezer.com/track/366297881',
    duration: 178,
    rank: 720016,
    explicit_lyrics: true,
    explicit_content_lyrics: 1,
    explicit_content_cover: 0,
    preview:
      'https://cdns-preview-e.dzcdn.net/stream/c-e40823133109b64049be9f81f2b632fa-4.mp3',
    contributors: [
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
    ],
    artist: {
      id: 8706544,
      name: 'Dua Lipa',
      tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 42194891,
      title: 'Dua Lipa (Deluxe)',
      cover: 'https://api.deezer.com/album/42194891/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/42194891/tracks',
      type: 'album',
    },
    type: 'track',
  },
  {
    id: 366297851,
    readable: true,
    title: 'Hotter Than Hell',
    title_short: 'Hotter Than Hell',
    title_version: '',
    link: 'https://www.deezer.com/track/366297851',
    duration: 187,
    rank: 687948,
    explicit_lyrics: false,
    explicit_content_lyrics: 0,
    explicit_content_cover: 0,
    preview:
      'https://cdns-preview-f.dzcdn.net/stream/c-f1d90c832981ee3f29c3409bd5bd9067-4.mp3',
    contributors: [
      {
        id: 8706544,
        name: 'Dua Lipa',
        link: 'https://www.deezer.com/artist/8706544',
        share:
          'https://www.deezer.com/artist/8706544?utm_source=deezer&utm_content=artist-8706544&utm_term=0_1551138741&utm_medium=web',
        picture: 'https://api.deezer.com/artist/8706544/image',
        picture_small:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/56x56-000000-80-0-0.jpg',
        picture_medium:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/250x250-000000-80-0-0.jpg',
        picture_big:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/500x500-000000-80-0-0.jpg',
        picture_xl:
          'https://e-cdns-images.dzcdn.net/images/artist/9af75263306ae53a0cc99e5da70cebc3/1000x1000-000000-80-0-0.jpg',
        radio: true,
        tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
        type: 'artist',
        role: 'Main',
      },
    ],
    artist: {
      id: 8706544,
      name: 'Dua Lipa',
      tracklist: 'https://api.deezer.com/artist/8706544/top?limit=50',
      type: 'artist',
    },
    album: {
      id: 42194891,
      title: 'Dua Lipa (Deluxe)',
      cover: 'https://api.deezer.com/album/42194891/image',
      cover_small:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/56x56-000000-80-0-0.jpg',
      cover_medium:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/250x250-000000-80-0-0.jpg',
      cover_big:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/500x500-000000-80-0-0.jpg',
      cover_xl:
        'https://e-cdns-images.dzcdn.net/images/cover/75476548cb1a37e557b38bd8e8f8823f/1000x1000-000000-80-0-0.jpg',
      tracklist: 'https://api.deezer.com/album/42194891/tracks',
      type: 'album',
    },
    type: 'track',
  },
];
