import React, { Component } from 'react';
import { Image, StyleSheet, View, Text, Animated, Dimensions } from 'react-native';

const PROFILE_IMAGE_HEIGHT = 70;
const IMG_URI = 'https://e-cdns-images.dzcdn.net/images/artist/193bca66c5fa26692c1a181c970922bb/56x56-000000-80-0-0.jpg';


export default class SmallHeader extends Component {

  render() {
    const { width : screenWidth } = Dimensions.get('window')
    const { opacity } = this.props
    return (
      <Animated.View           style={{
            backgroundColor: "black",
            position: "absolute",
            opacity: opacity,
            top: 0, 
            right: 0,
            left: 0,
            height: 160,
            overflow: 'hidden'
          }}>
          <View
          style={{
            height: PROFILE_IMAGE_HEIGHT,
            width: PROFILE_IMAGE_HEIGHT,
            borderRadius: PROFILE_IMAGE_HEIGHT / 2,
            borderColor: 'white',
            borderWidth: 3,
            top: 40, 
            left: (screenWidth - PROFILE_IMAGE_HEIGHT)/2,
            overflow: 'hidden'
          }}>
          <Image
            source={require('../assets/dualipa_profile.jpg') }
            style={{ flex: 1, width: null, height: null }}
          />
        </View>
      </Animated.View>

    );
  }
}

