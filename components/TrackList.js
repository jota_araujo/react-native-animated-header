import React from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  Platform,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import {
  material,
  systemWeights,
  materialColors,
} from 'react-native-typography';

import TrackListRow from './TrackListRow';

export default class TrackList extends React.Component {
  render() {
    console.log(this.props.tracks.length)
    return (
      <FlatList
        contentContainerStyle={styles.listContent}
        data={this.props.tracks}
        keyExtractor={item => item.song}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        ListHeaderComponent={() => (
          <Text style={styles.topTracks}>TOP TRACKS</Text>
        )}
        renderItem={({ item, index }) => (
          <TrackListRow
            number={index + 1}
            title={item.title}
            album={item.album.title}
            cover={item.album.cover_small}
            duration={item.duration}
          />
        )}
      />
    );
  }
}

const styles = StyleSheet.create({
  topTracks: {
    marginTop: 32,
    marginBottom: 16,
    marginHorizontal: 16,
    ...material.body2Object,
    color: materialColors.whitePrimary,
  },
  listContent: {
    justifyContent: 'flex-start',
    alignItems: 'stretch',
  },
  separator: {
    height: StyleSheet.hairlineWidth,
    width: '100%',
    backgroundColor: '#FFFFFF50',
  },
  icon: {
    backgroundColor: 'transparent',
    color: '#FFFFFF',
  },
});
