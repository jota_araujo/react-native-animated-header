import React from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  Platform,
  Image,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {  addSeconds, format } from 'date-fns';

import {
  material,
  systemWeights,
  materialColors,
} from 'react-native-typography';

const formattedDuration = seconds => {
  var helperDate = addSeconds(new Date(0), seconds);
  return format(helperDate, 'mm:ss');
}

export default ({ number, cover, title, album, duration }) => {
  return (
    <View style={rowStyles.row}>
      <Text style={rowStyles.number}>{number}</Text>
      <Image style={rowStyles.image} source={{ uri: cover }} />
      <View style={rowStyles.column}>
        <Text style={material.body2White}>{title}</Text>
        <Text style={material.captionWhite}>{album}</Text>
      </View>
      <Text style={rowStyles.duration}>{formattedDuration(duration)}</Text>
    </View>
  );
};

const constants = {
  PADDING_ANDROID: 16,
};

const rowStyles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: constants.PADDING_ANDROID,
    paddingTop: 4,
    paddingBottom: 8,
  },
  number: {
    ...material.body1Object,
    color: materialColors.whitePrimary,
  },
  image: {
    marginTop: 4,
    borderRadius: 3,
    marginLeft: constants.PADDING_ANDROID,
    height: 40,
    width: 40,
  },
  column: {
    flex: 1,
    marginLeft: constants.PADDING_ANDROID,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  duration: {
    ...material.body1Object,
    color: materialColors.whiteSecondary,
  },
});
